FROM openjdk:8-jdk-alpine
ENV JAVA_OPTS="-Xms512m -Xmx1024m"
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]