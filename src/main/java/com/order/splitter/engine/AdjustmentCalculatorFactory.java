package com.order.splitter.engine;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import com.order.splitter.util.AmountType;

/*
 * Factory returning instance of AdjustmentCalculator based on the requested type.
 */
@Service
public class AdjustmentCalculatorFactory {
	
	private static final Logger LOGGER = LogManager.getLogger(AdjustmentCalculatorFactory.class);
	
	private static final Map<Integer,AdjustmentCalculator> factoryMap =
		    Collections.unmodifiableMap(new HashMap<Integer,AdjustmentCalculator>() {
		    	
				private static final long serialVersionUID = 1L;

			{
		        put(AmountType.LINETOTAL.getCode(), new LineTotalAdjustmentCalculator());
		        put(AmountType.SHIPPING.getCode(), new ChargeDetailsAdjustmentCalculator());
		        put(AmountType.HANDLING.getCode(), new ChargeDetailsAdjustmentCalculator());
		        put(AmountType.MISC_CHARGE.getCode(), new ChargeDetailsAdjustmentCalculator());
		        put(AmountType.SHIPPING_TAX.getCode(), new TaxDetailAdjustmentCalculator());
		        put(AmountType.HANDLING_TAX.getCode(), new TaxDetailAdjustmentCalculator());
		        put(AmountType.SALES_TAX.getCode(), new TaxDetailAdjustmentCalculator());
		        put(AmountType.MISC_TAX.getCode(), new TaxDetailAdjustmentCalculator());
		        put(AmountType.DISCOUNTAMOUNT.getCode(), new DiscountAdjustmentCalculator());
		        put(AmountType.EXT_PURCHASE_PRICE.getCode(), new ExtendedPurchasePriceAdjustmentCalculator());
		        put(AmountType.EXT_PRICE.getCode(), new ExtendedPriceAdjustmentCalculator());
		    }});
	
	public AdjustmentCalculator createAdjustmentCalculator(Integer code) {
		return factoryMap.get(code);
	}
}
