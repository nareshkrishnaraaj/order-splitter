package com.order.splitter.engine;

import java.util.List;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.util.ProrationBean;

/*
 * Classes performing the proration on OrderLines must implement this interface.
 * The applyAdjustment method expects a Collection of same orderLines. i.e., All the orderLines are the
 * deep copies of the initial multi-quantity orderlines. 
 * If the initial quanity of an orderline is 5, the collection size is expected to be 5 copies of that orderLine
 * The type of proration must be updated in AmountType class
 * Update the AdjustmentCalculatorFactory accordingly.
 */
public interface AdjustmentCalculator {

	List<OrderLine> applyAdjustment(List<OrderLine> orderLines, ProrationBean proratedBean);
}
