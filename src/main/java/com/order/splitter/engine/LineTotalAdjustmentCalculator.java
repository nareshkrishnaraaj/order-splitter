package com.order.splitter.engine;

import java.math.RoundingMode;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.util.Constants;
import com.order.splitter.util.ProrationBean;

/*
 * Calcuates the adjustments for OrderLine Total
 */
public class LineTotalAdjustmentCalculator implements AdjustmentCalculator {

	private static final Logger LOGGER = LogManager.getLogger(LineTotalAdjustmentCalculator.class.getName());
	
	@Override
	public List<OrderLine> applyAdjustment(List<OrderLine> orderLines, ProrationBean proratedBean) {
		
		//Proration not required if the initial amount is 0.0
		if(proratedBean !=null && proratedBean.getAmountBeforeProration() !=null && 
				proratedBean.getAmountBeforeProration().compareTo(Constants.ZERO_VALUE)==0) {
			LOGGER.info("Amount before proration is 0.0. Skipping the proration");
			return orderLines;
		}
		boolean isDeltaCalculated = false;
		LOGGER.debug("Line Totoal before proration : {}",proratedBean.getAmountBeforeProration());
		for(OrderLine orderLine:orderLines) {
			if(!isDeltaCalculated && proratedBean.getDeltaAfterProration().compareTo(Constants.ZERO_VALUE) == 1) {
				orderLine.getLineTotal().
				setValue(proratedBean.getUnitAmountAfterProration().add(proratedBean.getDeltaAfterProration()).setScale(2, RoundingMode.FLOOR).toPlainString());
				isDeltaCalculated = true;
				LOGGER.debug("Line Totoal with Delta for line# {} : {}",orderLine.getLineNumber(),orderLine.getLineTotal().getValue());
			} else {
				orderLine.getLineTotal().setValue(proratedBean.getUnitAmountAfterProration().toPlainString());
				if(!isDeltaCalculated) {
					isDeltaCalculated=true;
				}
				LOGGER.debug("Line Totoal with No Delta for line# {} : {}",orderLine.getLineNumber(),orderLine.getLineTotal().getValue());
			}
		}
		LOGGER.info("LineTotalAdjust proration complete");
		return orderLines;
	}

}
