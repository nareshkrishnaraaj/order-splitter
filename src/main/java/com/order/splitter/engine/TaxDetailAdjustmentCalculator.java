package com.order.splitter.engine;

import java.math.RoundingMode;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.TaxDetails.TaxDetail;
import com.order.splitter.util.Constants;
import com.order.splitter.util.ProrationBean;

/*
 * Calculate Taxe adjustments (Sales, Shipping and Handling) for the orderlines.
 */
public class TaxDetailAdjustmentCalculator implements AdjustmentCalculator {

	private static final Logger LOGGER = LogManager.getLogger(TaxDetailAdjustmentCalculator.class.getName());

	@Override
	public List<OrderLine> applyAdjustment(List<OrderLine> orderLines, ProrationBean proratedBean) {

		if (proratedBean == null || proratedBean.getAmountType() == null
				|| proratedBean.getAmountType().getType() == null) {
			return orderLines;
		}

		// Proration not required if the initial amount is 0.0
		if (proratedBean != null && proratedBean.getAmountBeforeProration() != null
				&& proratedBean.getAmountBeforeProration().compareTo(Constants.ZERO_VALUE) == 0) {
			LOGGER.info("Amount before proration is 0.0. Skipping the proration");
			return orderLines;
		}

		boolean isDeltaCalculated = false;
		for (OrderLine orderLine : orderLines) {
			if (!isDeltaCalculated && proratedBean.getDeltaAfterProration().compareTo(Constants.ZERO_VALUE) == 1) {
				List<TaxDetail> taxDetails = orderLine.getTaxDetails().getTaxDetail();
				for (TaxDetail taxDetail : taxDetails) {
					if (taxDetail != null && taxDetail.getChargeCategory().getValue()
							.equalsIgnoreCase(proratedBean.getAmountType().getType())) {
						taxDetail.setTaxAmount(proratedBean.getUnitAmountAfterProration()
								.add(proratedBean.getDeltaAfterProration()).setScale(2, RoundingMode.FLOOR));
						LOGGER.debug("With Delta {} for  line# {} : {}", proratedBean.getAmountType().getType(),
								orderLine.getLineNumber(), taxDetail.getTaxAmount());
					}
				}
				isDeltaCalculated = true;
			} else {
				List<TaxDetail> taxDetails = orderLine.getTaxDetails().getTaxDetail();
				for (TaxDetail taxDetail : taxDetails) {
					if (taxDetail != null && taxDetail.getChargeCategory().getValue()
							.equalsIgnoreCase(proratedBean.getAmountType().getType())) {
						taxDetail.setTaxAmount(proratedBean.getUnitAmountAfterProration());
						LOGGER.debug("No Delta {} for  line# {} : {}", proratedBean.getAmountType().getType(),
								orderLine.getLineNumber(), taxDetail.getTaxAmount());
					}
				}
				if (!isDeltaCalculated) {
					isDeltaCalculated = true;
				}
			}
		}
		LOGGER.info("Tax proration complete for " + proratedBean.getAmountType().getType());
		return orderLines;
	}

}
