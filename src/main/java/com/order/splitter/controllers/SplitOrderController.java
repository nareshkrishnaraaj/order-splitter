package com.order.splitter.controllers;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.order.splitter.entities.TXML;
import com.order.splitter.exception.ServiceException;
import com.order.splitter.services.OrderSplitService;
import com.order.splitter.util.XMLUtil;

/*
 * REST Service for splitting multi-quantity order-lines to single quantity multi-orderlines.
 * If an Order with one orderline having quantity=3 is passed, the order is returned with 3 orderlines 
 * each having quantity 1. All the prices, promotions and taxes are prorated against 3 orderlines.
 */
@RestController
public class SplitOrderController {

	private static final Logger LOGGER = LogManager.getLogger(SplitOrderController.class);

	@Autowired
	OrderSplitService<TXML> splitService;
	
	@Autowired
	XMLUtil util;

	@RequestMapping(path = "/v1/order/split", method = RequestMethod.POST,
			produces = MediaType.APPLICATION_XML_VALUE,consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> splitOrder(@RequestBody String txml,@RequestHeader("ApiKey") String apiKey){
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		ThreadContext.push(UUID.randomUUID().toString());
		LOGGER.info("Initiating new request");
		TXML responseXml;
		try {
			responseXml = splitService.splitOrder(util.unmarshallRequest(txml));
			String responseBody = util.marshallRequest(responseXml);
			return new ResponseEntity<>(responseBody,HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error(e.getStackTrace());
			e.printStackTrace();
			throw new ServiceException(e);
		}
		finally {
			stopWatch.stop();
			LOGGER.info("Request completed in {} ms",stopWatch.getLastTaskTimeMillis());
			ThreadContext.pop();
		}
	}
}
