package com.order.splitter.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import com.order.splitter.entities.TXML;
import com.order.splitter.services.ItemCloneService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;

/*
 * Service to clone the requested objects using XStream library
 */
@Service
public class ItemCloneServiceImpl<T> implements ItemCloneService<T> {

	private static final Logger LOGGER = LogManager.getLogger(ItemCloneServiceImpl.class.getName());

	/**
	 * Returns a cloned instance of the requested object
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T clone(T t) {
		XStream xstream = XStreamInstanceLoader.CACHED_INSTANCE;
		String xml = xstream.toXML(t);
		T newObj = (T) xstream.fromXML(xml);
		return newObj;
	}

	/**
	 * Returns the Collection of cloned instances of the requested object
	 */
	@Override
	public List<T> clone(T t, int count) {
		if (count == 0) {
			return Collections.emptyList();
		}
		List<T> clonedObjectsList = new ArrayList<T>(count);
		while (count-- > 0) {
			clonedObjectsList.add(clone(t));
		}
		return clonedObjectsList;
	}

	private static class XStreamInstanceLoader {
		private static final XStream CACHED_INSTANCE;
		static {
			CACHED_INSTANCE = new XStream();
			CACHED_INSTANCE.addPermission(NoTypePermission.NONE);
			CACHED_INSTANCE.addPermission(NullPermission.NULL);
			CACHED_INSTANCE.addPermission(PrimitiveTypePermission.PRIMITIVES);
			CACHED_INSTANCE.allowTypeHierarchy(Collection.class);
			CACHED_INSTANCE.allowTypeHierarchy(String.class);
			CACHED_INSTANCE.allowTypeHierarchy(BigDecimal.class);
			// allow any type from the same
			CACHED_INSTANCE.allowTypesByWildcard(new String[] { TXML.class.getPackage().getName() + ".*" });
			LOGGER.info("XStream Instance is Cached now");
		}
	}
}
