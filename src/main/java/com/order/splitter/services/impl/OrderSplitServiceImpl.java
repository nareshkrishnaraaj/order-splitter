package com.order.splitter.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.splitter.entities.TXML;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.ChargeDetails.ChargeDetail;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.DiscountDetails.DiscountDetail;
import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine.TaxDetails.TaxDetail;
import com.order.splitter.exception.ServiceException;
import com.order.splitter.services.ItemSplitService;
import com.order.splitter.services.OrderSplitService;
import com.order.splitter.services.SplitQualifierService;
import com.order.splitter.util.Constants;


/**
 * Service to break-down the Order Line Items based on the defined rules and construct the complete order response after applying
 * the Split rules
 * @author Ismail
 *
 */
@Service
public class OrderSplitServiceImpl implements OrderSplitService<TXML> {

	private static final Logger LOGGER = LogManager.getLogger(OrderSplitServiceImpl.class.getName());
	
	@Autowired
	ItemSplitService<OrderLine> itemSplitService;
	
	@Autowired
	SplitQualifierService<OrderLine> qualifierService;
	
	/**
	 * Triggers the Order Split process based on the provided Order object and returns the final construct.
	 */
	@Override
	public TXML splitOrder(TXML order) {
		validateRequiredFields(order);
		List<OrderLine> orderLineItems = order.getMessage().getOrder().getOrderLines().getOrderLine();
		int orderLineSize = (orderLineItems != null)?orderLineItems.size():0;
		LOGGER.info("Number of order lines {}",orderLineSize);
		
		if(orderLineSize > 0) {
			List<OrderLine> orderLines = new ArrayList<OrderLine>(orderLineSize);
			for(OrderLine orderline: orderLineItems) {
				List<OrderLine> orderLinesProcessed = processOrderLine(orderline);
				orderLines.addAll(orderLinesProcessed);
			}
			LOGGER.info("Final size of orderlines {} ",orderLines.size());
			order.getMessage().getOrder().getOrderLines().getOrderLine().clear();

			//Resets the orderLineNumber with the new lineNumbers
			updateOrderLines(orderLines);

			order.getMessage().getOrder().getOrderLines().getOrderLine().addAll(orderLines);
			LOGGER.info("OrderLines updated to the original order");
		}
		return order;
	}
	
	private void validateRequiredFields(TXML order) {
		
		LOGGER.debug("Validating for the required fields");
		if(order == null || order.getMessage() == null || order.getMessage().getOrder() == null) {
			throw new ServiceException("Error while processing the order due to missing Order details");
		}
		
		String orderNumber = order.getMessage().getOrder().getOrderNumber()!=null?
				order.getMessage().getOrder().getOrderNumber().getValue():null;
		String storeOrderNumber = order.getMessage().getOrder().getExternalOrderNumber()!=null?
				order.getMessage().getOrder().getExternalOrderNumber().getValue():null;
		if(orderNumber==null && storeOrderNumber == null) {
			throw new ServiceException("Error while processing the order due to missing Order number");
		}
		LOGGER.info("Ecom Order# {}",orderNumber);
		LOGGER.info("XStore Order# {}",storeOrderNumber);
		
		if(order.getMessage().getOrder().getOrderLines() == null || 
				order.getMessage().getOrder().getOrderLines().getOrderLine() == null) {
			throw new ServiceException("Error while processing the order due to missing OrderLine details");
		}
	}
	
	/**
	 * Performs all the qualifier checks and the split operation at the Order-Line Item level.
	 * @param orderLine
	 * @return List<OrderLine>
	 */
	private List<OrderLine> processOrderLine(OrderLine orderLine){
		if(orderLine == null)
			return Collections.emptyList();
		
		List<OrderLine> orderLines = null;
		if(orderLine != null) {
			if(qualifierService.isItemQualified(orderLine)) {
				orderLines = itemSplitService.splitItem(orderLine);
			} else {
				orderLines = new ArrayList<OrderLine>(1);
				orderLines.add(orderLine);
			}
		}
		return orderLines;
	}
	
	private void updateChargeDetails(OrderLine orderLine, int lineNumberCounter) {
		
		if(orderLine.getChargeDetails() == null || orderLine.getChargeDetails().getChargeDetail() == null) {
			LOGGER.info("OrderLine "+orderLine.getLineNumber()+" Have no charge details");
			return;
		}
		List<ChargeDetail> chargDetails = orderLine.getChargeDetails().getChargeDetail();
		for(ChargeDetail detail:chargDetails) {
			if(detail != null && detail.getExtChargeDetailId() != null) {
				int index = detail.getExtChargeDetailId().indexOf("-");
				if(index != -1) {
					String prefix = detail.getExtChargeDetailId().substring(0, index+1);
					String newId = detail.getExtChargeDetailId().replace(prefix, String.valueOf(lineNumberCounter)+"-");
					detail.setExtChargeDetailId(newId);
				}
			}
		}
	}
	
	private void updateDiscountDetails(OrderLine orderLine, int lineNumberCounter) {
		
		if(orderLine.getDiscountDetails() == null || orderLine.getDiscountDetails().getDiscountDetail() == null) {
			LOGGER.info("OrderLine "+orderLine.getLineNumber()+" Has no Discount details");
			return;
		}
		//Update the ExtDiscountId, ExtDiscountDetailsId with the LineId assigned to the LineItem
		List<DiscountDetail> discountDetails = orderLine.getDiscountDetails().getDiscountDetail();
		if(discountDetails !=  null) {
			for(DiscountDetail discountDetail: discountDetails) {
				if(discountDetail != null) {
					if(discountDetail.getExtDiscountDetailId() != null) {
						int index = discountDetail.getExtDiscountDetailId().indexOf(Constants.HYPHEN);
						if(index != -1) {
							String prefix = discountDetail.getExtDiscountDetailId().substring(0, index+1);
							if(prefix != null && prefix.length()>0) {
								String newId = discountDetail.getExtDiscountDetailId().replace(prefix, String.valueOf(lineNumberCounter)+Constants.HYPHEN);
								discountDetail.setExtDiscountDetailId(newId);
							}
						}
					}
					if(discountDetail.getExtDiscountId() != null) {
						int index = discountDetail.getExtDiscountId().indexOf(Constants.HYPHEN);
						if(index != -1) {
							String prefix = discountDetail.getExtDiscountId().substring(0, index+1);
							if(prefix != null && prefix.length()>0) {
								String newId = discountDetail.getExtDiscountId().replace(prefix, String.valueOf(lineNumberCounter)+Constants.HYPHEN);
								discountDetail.setExtDiscountId(newId);
							}
						}
					}
				}
			}
		}
	}
	
	private void updateTaxDetails(OrderLine orderLine, int lineNumberCounter) {
		
		if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null) {
			LOGGER.info("OrderLine "+orderLine.getLineNumber()+" Have no Tax Details");
			return;
		}
		//Update the ExtTaxDetailId
		List<TaxDetail> taxDetails = orderLine.getTaxDetails().getTaxDetail();
		if(taxDetails != null) {
			for(TaxDetail taxDetail: taxDetails) {
				if(taxDetail != null && taxDetail.getExtTaxDetailId() != null) {
					int index = taxDetail.getExtTaxDetailId().indexOf("-");
					if(index != -1) {
						String prefix = taxDetail.getExtTaxDetailId().substring(0,index+1);
						String newId = taxDetail.getExtTaxDetailId().replace(prefix, String.valueOf(lineNumberCounter)+"-");
						taxDetail.setExtTaxDetailId(newId);
					}
				}
			}
		}
	}
	
	private void updateReferenceFields(OrderLine orderLine, int lineNumberCounter) {
		if(orderLine.getLineReferenceFields() == null || orderLine.getLineReferenceFields().getReferenceField1() == null) {
			LOGGER.info("OrderLine "+orderLine.getLineNumber()+" Have no Reference field1");
			return;
		}
		
		//update LineReferenceFields
		if(orderLine.getLineReferenceFields().getReferenceField1().getValue() != null) {
			orderLine.getLineReferenceFields().getReferenceField1().
			setValue(String.format("%08d", Integer.valueOf(lineNumberCounter)).concat("00000"));
		}
	}
	
	/**
	 * The splitted order is updated with the new OrderLine Ids.
	 * The new orderLine# are updated accordingly to the Shipping,Discount and Tax details details
	 * @param orderLines
	 */
	private void updateOrderLines(List<OrderLine> orderLines) {
		if(orderLines != null) {
			int lineNumberCounter = 1;
			for(OrderLine orderLine: orderLines) {
				if(orderLine != null) {
					orderLine.getQuantity().setOrderedQty(Constants.ONE_VALUE);
					orderLine.setLineNumber(String.valueOf(lineNumberCounter));
						updateChargeDetails(orderLine, lineNumberCounter);
						updateDiscountDetails(orderLine, lineNumberCounter);
						updateTaxDetails(orderLine, lineNumberCounter);
						updateReferenceFields(orderLine,lineNumberCounter);
				}
				lineNumberCounter++;
			}
		}
	}
}
