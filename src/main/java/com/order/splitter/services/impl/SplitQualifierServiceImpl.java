package com.order.splitter.services.impl;

import com.order.splitter.services.SplitQualifierService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.order.splitter.entities.TXML.Message.Order.OrderLines.OrderLine;

/*
 * The criteria for qualification is atleast one multi-quantity orderline.
 */
@Service
public class SplitQualifierServiceImpl extends SplitQualifierService<OrderLine> {
	
	private static final Logger LOGGER = LogManager.getLogger(SplitQualifierServiceImpl.class.getName());

	@Override
	protected boolean applyQualifierRules(OrderLine orderLine) {
		
		return isMultiQuantityItem(orderLine);
	}
	
	/**
	 * Line item qualifies for split if the unit count is above one
	 * @param orderLine
	 * @return
	 */
	private boolean isMultiQuantityItem(OrderLine orderLine) {
		
		if(orderLine == null) {
			return Boolean.FALSE;
		}
		Float qty = orderLine.getQuantity().getOrderedQty().floatValue();
		if(qty.intValue() > 1) {
			LOGGER.debug("Orderline# {} qualifies for split",orderLine.getLineNumber());
			return Boolean.TRUE;
		} else {
			LOGGER.debug("Orderline# {} does not qualifies for split",orderLine.getLineNumber());
			return Boolean.FALSE;
		}
	}

}
