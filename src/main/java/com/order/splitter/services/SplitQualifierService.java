package com.order.splitter.services;

/*
 * Each qualifying criteria must extends this class and override the  applyQualifierRules method.
 */
public abstract class  SplitQualifierService<T> {


	protected abstract boolean applyQualifierRules(T t);


	public boolean isItemQualified(T t) {
		return applyQualifierRules(t);
	}
}
