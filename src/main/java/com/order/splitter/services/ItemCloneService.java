package com.order.splitter.services;

import java.util.List;

public interface ItemCloneService<T> {
	
	T clone(T t);
	List<T> clone(T t, int count);

}
