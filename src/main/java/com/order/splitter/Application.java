package com.order.splitter;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class Application {
	
	private static final Logger logger = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args) {
  
        SpringApplication.run(Application.class, args);
        logger.info("Initialized the application");
    }
}
