package com.order.splitter.util;

public enum AmountType {

	LINETOTAL("LineTotal",0),
	SHIPPING("Shipping",1),
	HANDLING("Handling",2),
	DISCOUNTAMOUNT("DiscountAmount",3),
	SALES_TAX("",4),
	SHIPPING_TAX("Shipping",5),
	HANDLING_TAX("Handling",6),
	MISC_TAX("Misc",7),
	EXT_PURCHASE_PRICE("ExtendedPurchasePrice",8),
	EXT_PRICE("ExtendedPrice",9),
	MISC_CHARGE("Misc",10);
	
	
	private String type;
	private int code;
	
	AmountType(String type, int code){
		this.type=type;
		this.code=code;
	}
	
	public String getType() {
		return this.type;
	}
	
	public int getCode() {
		return this.code;
	}
}