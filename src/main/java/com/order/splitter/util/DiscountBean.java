package com.order.splitter.util;

public final class DiscountBean {
	
	String discountType;
	String  discountId;
	
	public DiscountBean(String discountType,String  discountId){
		this.discountId=discountId;
		this.discountType=discountType;
	}

	/**
	 * @return the discountType
	 */
	public String getDiscountType() {
		return discountType;
	}

	/**
	 * @param discountType the discountType to set
	 */
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	/**
	 * @return the discountId
	 */
	public String getDiscountId() {
		return discountId;
	}

	/**
	 * @param discountId the discountId to set
	 */
	public void setDiscountId(String discountId) {
		this.discountId = discountId;
	}
}
