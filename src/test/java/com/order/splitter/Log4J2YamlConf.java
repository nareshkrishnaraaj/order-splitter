package com.order.splitter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;


public class Log4J2YamlConf {
	private static Logger logger = LogManager.getLogger();
    
	@Test
	public void performSomeTask(){
    	System.out.println(logger.getLevel());
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");
    }
    

}
